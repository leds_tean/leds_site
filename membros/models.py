import re
from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager, BaseUserManager
from imagekit.models import ProcessedImageField
from pilkit.processors import ResizeToFill


class Associados(AbstractBaseUser, PermissionsMixin):
    """
    Associados: classe com os membros que sao associados ao LEDS
    MEMBER_STATUS_CHOICES: uma tupla com os possiveis tipos de membro do LEDS
    status_member_type: represetacao numerica do MEMBER_STATUS_CHOICES
    is_is_staff: para saber se o usuario é admin do site
    """
    username = models.CharField('Usuário', max_length=30, unique=True,
        validators=[validators.RegexValidator(re.compile('^[\w.@+-]+$'),
        'O nome de usuário só pode conter letras, digitos ou os '
        'seguintes caracteres: @/./+/-/_', 'invalid')]
    )
    name = models.CharField('nome', max_length=100)
    email = models.EmailField('E-mail', unique=True)
    MEMBER_STATUS_CHOICES = ((0, 'Bolsista'), (1, 'Orientador'), (2, 'Associado'))
    MEMBER_SEXO = ((0, 'Masculino'), (1, 'Feminino'))
    status_member_type = models.IntegerField('Tipo de membro', choices=MEMBER_STATUS_CHOICES, default=2, blank=True)
    sexo_member = models.IntegerField('Sexo', choices=MEMBER_SEXO, default=0, blank=True)
    instituicao = models.CharField('instituição', max_length=200)
    profile_image = ProcessedImageField(upload_to='media/img/%Y/%m/%d', processors=[ResizeToFill(150, 150)],
                                        format='JPEG', options={'quality': 60}, blank=True)#compressao de imagem
    slug = models.SlugField("Slug", max_length=100, blank=True, unique=True)
    interesses = models.CharField("Interesse", max_length=300, blank=True)
    lattes = models.CharField("Curriculo Lattes", max_length=300, blank=True)
    site_url = models.CharField("Site Pessoal", max_length=300, blank=True)
    is_active = models.BooleanField('Está ativo?', blank=True, default=True)
    is_staff = models.BooleanField('É administrador?', blank=True, default=False)
    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    @models.permalink
    def get_absolute_url(self):
        return 'membros:perfil', (), {'slug': self.slug}

    def __str__(self):
        return self.name

    def get_short_name(self):
        return str(self)

    def get_full_name(self):
        return self.name

    class Meta:
        verbose_name = 'Associado'
        verbose_name_plural = 'Associados'


class Parceiros(models.Model):
    """
    Parceiros: classe com os parceiros do LEDS
    image1~3: imagens para o banner dos parceiros
    url_image1~3: url para a imagem dos baners
    """
    nome = models.CharField('nome', max_length=100)
    email = models.EmailField('E-mail', unique=True, blank=True)
    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)
    image1 = models.ImageField('Imagem1', upload_to='media/img/%Y/%m/%d', blank=True)
    image2 = models.ImageField('Imagem2', upload_to='media/img/%Y/%m/%d', blank=True)
    image3 = models.ImageField('Imagem3', upload_to='media/img/%Y/%m/%d', blank=True)
    url_image1 = models.URLField('url_image1', blank=True)
    url_image2 = models.URLField('url_image2', blank=True)
    url_image3 = models.URLField('url_image3', blank=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Parceiro'
        verbose_name_plural = 'Parceiros'