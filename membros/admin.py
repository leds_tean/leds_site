from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from .models import Associados, Parceiros
from .forms import UserChangeForm, UserCreationForm


class AssociadosAdmin(UserAdmin):
    """
    list_display: quais os campus serao mostrado no admin do site
    search_fields: indica os campus que serao usados para pesquisa
    form: set o formulario que sera usado na pagina do admin
    add_forms: set o formulario que sera usado na pagina do admin para alteracao do usuario
    """
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ['username', 'name', 'slug', 'email', 'status_member_type', 'profile_image', 'sexo_member', 'instituicao', 'is_staff']
    list_filter = ['username', 'email']
    fieldsets = [[None, {'fields': ['username', 'email', 'password', 'slug']}],
                 ['Personal Info', {'fields': ('name', 'status_member_type', 'instituicao', 'sexo_member', 'profile_image')}],
                 ['Permissions', {'fields': ('is_staff',)}],
                 ]
    add_fieldsets = [[None, {'classes': ('wide',),
                     'fields': ('username', 'name', 'slug', 'email', 'password1', 'password2', 'sexo_member', 'status_member_type',
                                'profile_image', 'instituicao', 'is_staff')}], ]

    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('username', 'email')
    ordering = ('username',)


class ParceirosAdmin(admin.ModelAdmin):
    """
    filter_horizontal: lista os participantes de uma forma melhor para ser adicionados
    render_change_form: metodo que altera as opcoes do select box de cordenador no admin de projetos
                        limitando as opcoes para apanas orientadores
    """

    list_display = ['nome', 'email', 'image1', 'image2', 'image3', 'url_image1', 'url_image2', 'url_image3']
    search_fields = ['nome']

admin.site.register(Associados, AssociadosAdmin)
admin.site.register(Parceiros, ParceirosAdmin)
