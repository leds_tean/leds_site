# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='sexo_member',
            field=models.IntegerField(default=0, blank=True, verbose_name='Sexo', choices=[(0, 'Masculino'), (1, 'Feminino')]),
            preserve_default=True,
        ),
    ]
