# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='status_member_type',
            field=models.IntegerField(blank=True, default=2, choices=[(0, 'Bolsista'), (1, 'Orientador'), (2, 'Associado')], verbose_name='Tipo de membro'),
            preserve_default=True,
        ),
    ]
