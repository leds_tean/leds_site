# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0009_auto_20150419_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='lattes',
            field=models.CharField(blank=True, max_length=300, verbose_name='Curriculo Lattes'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='site_url',
            field=models.CharField(blank=True, max_length=300, verbose_name='Site Pessoal'),
            preserve_default=True,
        ),
    ]
