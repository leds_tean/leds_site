# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0002_auto_20150409_2110'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='profile_image',
            field=models.ImageField(upload_to='media/img/%Y/%m/%d', blank=True, verbose_name='Imagem do perfil'),
            preserve_default=True,
        ),
    ]
