# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0007_associados_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='interesses',
            field=models.CharField(max_length=300, default='', verbose_name='Interesse'),
            preserve_default=False,
        ),
    ]
