# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Associados',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='nome')),
                ('email', models.EmailField(max_length=75, unique=True, verbose_name='E-mail')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='Data de Entrada')),
            ],
            options={
                'verbose_name_plural': 'Associados',
                'verbose_name': 'Associado',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Parceiros',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='nome')),
                ('email', models.EmailField(max_length=75, unique=True, verbose_name='E-mail')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='Data de Entrada')),
            ],
            options={
                'verbose_name_plural': 'Parceiros',
                'verbose_name': 'Parceiro',
            },
            bases=(models.Model,),
        ),
    ]
