# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='instituicao',
            field=models.CharField(verbose_name='instituição', max_length=200, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='image1',
            field=models.ImageField(verbose_name='Imagem1', blank=True, upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='image2',
            field=models.ImageField(verbose_name='Imagem2', blank=True, upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='image3',
            field=models.ImageField(verbose_name='Imagem3', blank=True, upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='url_image1',
            field=models.URLField(verbose_name='url_image1', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='url_image2',
            field=models.URLField(verbose_name='url_image2', default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parceiros',
            name='url_image3',
            field=models.URLField(verbose_name='url_image3', default=''),
            preserve_default=False,
        ),
    ]
