# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0012_auto_20150423_1649'),
    ]

    operations = [
        migrations.RenameField(
            model_name='parceiros',
            old_name='name',
            new_name='nome',
        ),
        migrations.AlterField(
            model_name='parceiros',
            name='email',
            field=models.EmailField(verbose_name='E-mail', unique=True, blank=True, max_length=75),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parceiros',
            name='url_image1',
            field=models.URLField(verbose_name='url_image1', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parceiros',
            name='url_image2',
            field=models.URLField(verbose_name='url_image2', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parceiros',
            name='url_image3',
            field=models.URLField(verbose_name='url_image3', blank=True),
            preserve_default=True,
        ),
    ]
