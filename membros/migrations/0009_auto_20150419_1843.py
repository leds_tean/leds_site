# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0008_associados_interesses'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='interesses',
            field=models.CharField(blank=True, max_length=300, verbose_name='Interesse'),
            preserve_default=True,
        ),
    ]
