# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0011_auto_20150423_1648'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='profile_image',
            field=models.ImageField(verbose_name='Imagem do perfil', upload_to='media/img/%Y/%m/%d', blank=True),
            preserve_default=True,
        ),
    ]
