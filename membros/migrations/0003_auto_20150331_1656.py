# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import re
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('membros', '0002_associados_status_member_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='groups',
            field=models.ManyToManyField(verbose_name='groups', related_name='user_set', help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', blank=True, related_query_name='user', to='auth.Group'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='Está ativo?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='is_staff',
            field=models.BooleanField(default=False, verbose_name='É administrador?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='is_superuser',
            field=models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='last_login',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='password',
            field=models.CharField(default='', verbose_name='password', max_length=128),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='associados',
            name='user_permissions',
            field=models.ManyToManyField(verbose_name='user permissions', related_name='user_set', help_text='Specific permissions for this user.', blank=True, related_query_name='user', to='auth.Permission'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='associados',
            name='username',
            field=models.CharField(validators=[django.core.validators.RegexValidator(re.compile('^[\\w.@+-]+$', 32), 'O nome de usuário só pode conter letras, digitos ou os seguintes caracteres: @/./+/-/_', 'invalid')], default='', verbose_name='Usuário', max_length=30, unique=True),
            preserve_default=False,
        ),
    ]
