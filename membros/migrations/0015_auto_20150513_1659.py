# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0014_auto_20150502_1541'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='profile_image',
            field=imagekit.models.fields.ProcessedImageField(blank=True, upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
    ]
