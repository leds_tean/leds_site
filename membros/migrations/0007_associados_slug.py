# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0006_auto_20150415_2002'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='slug',
            field=models.SlugField(max_length=100, unique=True, verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
    ]
