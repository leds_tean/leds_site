# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0003_associados_profile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='associados',
            name='sexo_member',
            field=models.IntegerField(default=1, choices=[(0, 'Masculino'), (1, 'Feminino')], verbose_name='Sexo', blank=True),
            preserve_default=False,
        ),
    ]
