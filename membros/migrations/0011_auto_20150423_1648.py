# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0010_auto_20150419_1944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='profile_image',
            field=models.FileField(blank=True, verbose_name='Imagem do perfil', upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
    ]
