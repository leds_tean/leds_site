# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('membros', '0013_auto_20150428_2349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associados',
            name='profile_image',
            field=imagekit.models.fields.ProcessedImageField(upload_to='media/img/%Y/%m/%d'),
            preserve_default=True,
        ),
    ]
