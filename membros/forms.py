from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from membros.models import Associados


class UserCreationForm(forms.ModelForm):
    """
    UserCreationForm: classe para criacao de usuario na pag do admin
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Associados
        fields = ('username', 'name', 'slug', 'email', 'status_member_type', 'profile_image', 'sexo_member', 'instituicao', 'is_staff')
        exclude = ()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("As senhas estão diferentes")

        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])

        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """
    UserChangeForm: classe para fazer alteracao no usuario na pagina de admin
    """
    password = ReadOnlyPasswordHashField(label= ("Password"),
                help_text= ("Raw passwords are not stored, so there is no way to see "
                "this user's password, but you can change the password "
                "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = Associados

    def clean_password(self):
        return self.initial["password"]


class PerfilForm(forms.Form):
    nome = forms.CharField(label="Nome", max_length=100, required=False,
                           widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}), required=False)
    MEMBER_SEXO = ((0, 'Masculino'), (1, 'Feminino'))
    sexo_member = forms.ChoiceField(label='Sexo', choices=MEMBER_SEXO, required=False,
                                    widget=forms.Select(attrs={'class': 'form-control'}))
    instituicao = forms.CharField(label='instituição', max_length=200, required=False,
                                  widget=forms.TextInput(attrs={'class': 'form-control'}))
    profile_image = forms.ImageField(label='Alterar Imagem', required=False)
    interesses = forms.CharField(label="Interesse", required=False,
                                 widget=forms.Textarea(attrs={'class': 'form-control'}))
    lattes = forms.URLField(label="Curriculo Lattes", required=False,
                            widget=forms.URLInput(attrs={'class': 'form-control'}))
    site_url = forms.URLField(initial='http://', required=False,
                              widget=forms.URLInput(attrs={'class': 'form-control'}))