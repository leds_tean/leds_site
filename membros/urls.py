from django.conf.urls import patterns, url

urlpatterns = patterns('membros.views',
    url(r'^associados/$', 'show_associados', name='associados'),
    url(r'^associados/(?P<slug>[\w_-]+)/$', 'perfil', name='perfil'),
    url(r'^associados-submit/$', 'update_perfil', name='update_perfil'),
    url(r'^parceiros/$', 'show_parceiros', name='parceiros')

)
