from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect

from .models import Associados, Parceiros
from .forms import PerfilForm
from projetos.models import Projeto
from doctecnicos.models import DocumentosTecnicos
from publicacoes.models import Publicacoes


def show_associados(request):
    associados = Associados.objects.filter().order_by('name')
    projetos = Projeto.objects.filter()
    template = 'associados.html'
    context = {'associados': associados, 'projetos': projetos}
    return render(request, template, context)


def perfil(request, slug):
    perfil_descricao = get_object_or_404(Associados, slug=slug)
    doc = DocumentosTecnicos.objects.filter(propietario__id__contains=perfil_descricao.id)
    pub = Publicacoes.objects.filter(autor_principal__id__contains=perfil_descricao.id)
    pub_co = Publicacoes.objects.filter(autor_secundario__id__contains=perfil_descricao.id)
    data = {}
    if request.method == 'POST':
        form = PerfilForm(request.POST, request.FILES)
        if form.is_valid():
            model_perfil = perfil_descricao
            model_perfil.name = form.cleaned_data['nome']
            model_perfil.email = form.cleaned_data['email']
            model_perfil.sexo_member = form.cleaned_data['sexo_member']
            model_perfil.instituicao = form.cleaned_data['instituicao']
            # model_perfil.profile_image = request.FILES['profile_image']
            model_perfil.interesses = form.cleaned_data['interesses']
            model_perfil.lattes = form.cleaned_data['lattes']
            model_perfil.site_url = form.cleaned_data['site_url']
            model_perfil.save()
            data['status'] = "ok"
            messages.success(request, "Perfil Atualizado com Sucesso!")
            return JsonResponse(data)
        else:
            data['erro'] = form.errors
            return JsonResponse(data)
    else:
        form = PerfilForm(
            initial={
                'nome': perfil_descricao.name,
                'email': perfil_descricao.email,
                'sexo_member': perfil_descricao.sexo_member,
                'instituicao': perfil_descricao.instituicao,
                'profile_image': perfil_descricao.profile_image,
                'interesses': perfil_descricao.interesses,
                'lattes': perfil_descricao.lattes,
                'site_url': perfil_descricao.site_url,
            }
        )

    template = 'perfil.html'
    context = {'form': form, 'perfil_info': perfil_descricao, 'documentos': doc, 'publicacoes': pub,
               'publicacoes_co': pub_co
    }

    return render(request, template, context)


def update_perfil(request):
    if request.method == 'POST':
        form = PerfilForm(request.POST, request.FILES)
        if form.is_valid():
            model_perfil = Associados(id=request.POST.get('id'))
            model_perfil.name = form.cleaned_data['nome']
            model_perfil.email = form.cleaned_data['email']
            model_perfil.sexo_member = form.cleaned_data['sexo_member']
            model_perfil.instituicao = form.cleaned_data['instituicao']
            model_perfil.profile_image = form.cleaned_data['profile_image']
            model_perfil.interesses = form.cleaned_data['interesses']
            model_perfil.lattes = form.cleaned_data['lattes']
            model_perfil.site_url = form.cleaned_data['site_url']
            model_perfil.save()
            return redirect('/associados/', model_perfil.slug)
    else:
        form = PerfilForm()
    template = 'perfil.html'
    context = {'form': form}

    return render(request, template, context)


def show_parceiros(request):
    parceiros = Parceiros.objects.filter().order_by('nome')
    template = 'parceiros.html'
    context = {'parceiros': parceiros}
    return render(request, template, context)