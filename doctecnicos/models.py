from django.db import models
from membros.models import Associados


class DocumentosTecnicos(models.Model):
    nome = models.CharField('Nome', max_length=100)
    descricao = models.TextField('Descrição', blank=True)
    arquivo = models.FileField('Arquivo', upload_to='media/pdf/%Y/%m/%d')
    propietario = models.ForeignKey(Associados, verbose_name='Propietário', related_name='proprietáio')

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Documento Técnico'
        verbose_name_plural = 'Documentos Técnicos'