from django.conf.urls import patterns, url

urlpatterns = patterns('doctecnicos.views',
    url(r'^submeter-documentos-tecnicos/$', 'submeter_documento', name='submit-documentos-tecnicos'),
    url(r'^documentos-tecnicos/$', 'lista_documentos', name='documentos-tecnicos'),

)
