# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0007_auto_20150330_1601'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentostecnicos',
            name='arquivo',
            field=models.FileField(upload_to='/pdf/%Y/%m/%d', default='', verbose_name='Arquivo'),
            preserve_default=False,
        ),
    ]
