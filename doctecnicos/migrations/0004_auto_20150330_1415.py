# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0003_auto_20150329_1820'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentostecnicos',
            name='arquivo',
            field=models.FileField(upload_to='/pdf/%Y/%m/%d', verbose_name='Arquivo'),
            preserve_default=True,
        ),
    ]
