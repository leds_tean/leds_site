# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0009_auto_20150330_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentostecnicos',
            name='descricao',
            field=models.TextField(verbose_name='Descrição', blank=True),
            preserve_default=True,
        ),
    ]
