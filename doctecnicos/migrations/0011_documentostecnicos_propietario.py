# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('doctecnicos', '0010_documentostecnicos_descricao'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentostecnicos',
            name='propietario',
            field=models.ForeignKey(verbose_name='Propietário', default=1, to=settings.AUTH_USER_MODEL, related_name='proprietáio'),
            preserve_default=False,
        ),
    ]
