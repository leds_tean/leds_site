# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0002_auto_20150329_1820'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentostecnicos',
            name='descricao',
            field=models.TextField(blank=True, verbose_name='Descrição'),
            preserve_default=True,
        ),
    ]
