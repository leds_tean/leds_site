# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentostecnicos',
            name='descricao',
            field=models.CharField(verbose_name='Descrição', max_length=1000),
            preserve_default=True,
        ),
    ]
