# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0004_auto_20150330_1415'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documentostecnicos',
            name='descricao',
        ),
    ]
