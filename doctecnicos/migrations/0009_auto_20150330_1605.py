# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0008_documentostecnicos_arquivo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentostecnicos',
            name='arquivo',
            field=models.FileField(verbose_name='Arquivo', upload_to='media/pdf/%Y/%m/%d'),
            preserve_default=True,
        ),
    ]
