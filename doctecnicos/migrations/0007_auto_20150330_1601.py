# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0006_remove_documentostecnicos_nome'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documentostecnicos',
            name='arquivo',
        ),
        migrations.AddField(
            model_name='documentostecnicos',
            name='nome',
            field=models.CharField(verbose_name='Nome', max_length=100, default=''),
            preserve_default=False,
        ),
    ]
