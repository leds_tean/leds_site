# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('doctecnicos', '0005_remove_documentostecnicos_descricao'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='documentostecnicos',
            name='nome',
        ),
    ]
