from django import forms


class DocumentosTecForm(forms.Form):
    arquivo = forms.FileField(label='Selecione um arquivo',
                              widget=forms.FileInput(attrs={'accept': 'application/pdf'}))
    nomeDocumento = forms.CharField(label='Nome do documento', max_length=100,
                                    widget=forms.TextInput(attrs={'class': 'form-control'}))
    descricaoDocumento = forms.CharField(label='Descrição do Documento',
                                         widget=forms.Textarea(attrs={'class': 'form-control'}), required=False)