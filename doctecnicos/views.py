from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .forms import DocumentosTecForm
from .models import DocumentosTecnicos


@login_required
def submeter_documento(request):
    if request.method == 'POST':
        form = DocumentosTecForm(request.POST, request.FILES)
        if form.is_valid():
            model_doc = DocumentosTecnicos()
            model_doc.nome = form.cleaned_data['nomeDocumento']
            model_doc.descricao = form.cleaned_data['descricaoDocumento']
            model_doc.arquivo = request.FILES['arquivo']
            model_doc.propietario = request.user
            model_doc.save()
            form = DocumentosTecForm()
            messages.success(request, "Documento Enviado com sucesso!")
    else:
        form = DocumentosTecForm()
    template = 'submit_dt.html'
    context = {'form': form}

    return render(request, template, context)


def lista_documentos(request):
    documentos_list = DocumentosTecnicos.objects.all()
    paginator = Paginator(documentos_list, 10)

    page = request.GET.get('page')
    try:
        documentos = paginator.page(page)
    except PageNotAnInteger:
        documentos = paginator.page(1)
    except EmptyPage:
        documentos = paginator.page(paginator.num_pages)

    template = 'documentos_tecnicos.html'
    context = {'documentos': documentos, 'paginator': paginator}
    return render(request, template, context)
