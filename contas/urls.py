from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^entrar/$', 'django.contrib.auth.views.login', {'template_name': 'contas/login.html'}, name='login'),
    url(r'^sair/$', 'django.contrib.auth.views.logout', {'next_page': 'noticias:index'}, name='logout'),
    url(r'^editar-senha/$', 'contas.views.edit_password', name='edit_password'),
)
