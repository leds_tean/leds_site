from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('',
    url(r'^$', include('noticias.urls', namespace='noticias')),
    url(r'^conta/', include('contas.urls', namespace='contas')),
    url(r'^documentos-tecnicos/', include('doctecnicos.urls', namespace='doctecnicos')),
    url(r'^quem-somos/', include('membros.urls', namespace='membros')),
    # url(r'^$', include('noticias.urls', namespace='home')),
    url(r'^noticias/', include('noticias.urls', namespace='noticias')),
    url(r'^projetos/', include('projetos.urls', namespace='projetos')),
    url(r'^publicacoes/', include('publicacoes.urls', namespace='publicacoes')),
    url(r'^relatorio/', include('relatorio.urls', namespace='relatorio')),
    url(r'^quem-somos/', include('leds.urls', namespace='quemSomos')),
    url(r'^core/', include('core.urls', namespace='core')),#apagar depois

    # chamado no template base para exibir o perfil do associado ao clicar sobre o seu nome
    url(r'^quem-somos/associados/(?P<slug>[\w_-]+)/$', 'membros.views.perfil', name='perfil'),

    url(r'^redactor/', include('redactor.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)