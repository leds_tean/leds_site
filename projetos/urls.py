from django.conf.urls import patterns, url

urlpatterns = patterns('projetos.views',
    url(r'^$', 'projetos', name='index'),
    url(r'^descricao/', 'status', name='descricao'),
)
