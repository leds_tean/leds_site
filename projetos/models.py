from django.db import models
from membros.models import Associados
from redactor.fields import RedactorField


class Projeto(models.Model):

    TIPO_PUBLICACOES_CHOICES = ((0, 'Em Andamento'), (1, 'Finalizado'), (2, 'Concluido'))
    title = models.CharField('titulo', max_length=300)
    status = models.IntegerField('Status', choices=TIPO_PUBLICACOES_CHOICES, blank=True)
    sub_title = models.CharField('sub-titulo', max_length=100, blank=True)
    descricao = RedactorField(verbose_name=u'Descriçao', redactor_options={'plugins': ['table']})
    cordenador = models.ForeignKey(Associados, verbose_name='Coordenador', related_name='cordenador_projeto')
    participantes = models.ManyToManyField(Associados, verbose_name='Participantes', related_name='participantes_projetos')

    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Projeto'
        verbose_name_plural = 'Projetos'
