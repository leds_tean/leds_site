from django.shortcuts import render, get_object_or_404
from .models import Projeto


def projetos(request):
    noticias_home = Projeto.objects.all()
    template_name = 'projeto.html'
    context = {
        'projetos': noticias_home
    }

    return render(request, template_name, context)


def status(request):
    noticias_home = Projeto.objects.all()

    template_name = 'projeto_descricao.html'
    context = {
        'projetos': noticias_home
    }

    return render(request, template_name, context)
