# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projetos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projeto',
            name='status',
            field=models.IntegerField(verbose_name='Status', choices=[(0, 'Em Andamento'), (1, 'Finalizado'), (2, 'Concluido')], blank=True),
            preserve_default=True,
        ),
    ]
