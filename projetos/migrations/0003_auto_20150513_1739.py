# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projetos', '0002_auto_20150513_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projeto',
            name='title',
            field=models.CharField(max_length=300, verbose_name='titulo'),
            preserve_default=True,
        ),
    ]
