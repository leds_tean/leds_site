# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Projeto',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=100, verbose_name='titulo')),
                ('status', models.IntegerField(verbose_name='Status', blank=True, choices=[(0, 'Iniciado'), (1, 'Em Andamento'), (2, 'Finalizado'), (3, 'Concluido')])),
                ('sub_title', models.CharField(max_length=100, verbose_name='sub-titulo', blank=True)),
                ('descricao', redactor.fields.RedactorField(verbose_name='Descriçao')),
                ('date_joined', models.DateTimeField(verbose_name='Data de Entrada', auto_now_add=True)),
                ('cordenador', models.ForeignKey(verbose_name='Coordenador', to=settings.AUTH_USER_MODEL, related_name='cordenador_projeto')),
                ('participantes', models.ManyToManyField(verbose_name='Participantes', to=settings.AUTH_USER_MODEL, related_name='participantes_projetos')),
            ],
            options={
                'verbose_name': 'Projeto',
                'verbose_name_plural': 'Projetos',
            },
            bases=(models.Model,),
        ),
    ]
