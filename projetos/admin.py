from django.contrib import admin
from .models import Associados
from .models import Projeto


class ProjetoAdmin(admin.ModelAdmin):
    """
    filter_horizontal: lista os participantes de uma forma melhor para ser adicionados
    render_change_form: metodo que altera as opcoes do select box de cordenador no admin de projetos
                        limitando as opcoes para apanas orientadores
    """

    list_display = ['title', 'sub_title', 'status', 'cordenador', 'date_joined']
    search_fields = ['title']
    filter_horizontal = ('participantes',)

    def render_change_form(self, request, context, *args, **kwargs):
        context['adminform'].form.fields['cordenador'].queryset = Associados.objects.filter(status_member_type=1)
        return super(ProjetoAdmin, self).render_change_form(request, context, args, kwargs)

admin.site.register(Projeto, ProjetoAdmin)

