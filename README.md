# README #

Destinado apenas para os membros da equipe.
Aqui será descrito as funcionalidades de cada membro da equipe.

## ANDRÉ: ##
* Realizar o levantamento de requesitos;
* Configura o Framework Django;
* Fazer o planejamento das atividades do estágio;
* Desenvolver o módulo de administração do site;
* Desenvolver o módulo de interfaze de usuário administrador utilizando o framework Bootstrap;
* Desenvolver os CRUDS de associados, documentos técnicos e parceiros;

## ÁLVARO: ##
* Realizar o levantamento de requesitos;
* Configura o Framework Django;
* Fazer o planejamento das atividades do estágio;
* Desenvolver o módulo da biblioteca virtual;
* Desenvolver o módulo de interface de usuário bolsistas com a utilização do framework Bootstrap;
* Desenvolver os CRUDS de noticias, eventos e projetos.

## EDSON: ##
* Realizar o levantamento de requesitos;
* Configura o Framework Django;
* Fazer o planejamento das atividades do estágio;
* Responsável pela parte do ORM do Django;
* Desenvolver o módulo de cronograma;
* Desenvolver o módulo de interface de usuários externos com a utilização do framework Bootstrap;
* Desenvolver os CRUDS de atas, quem somos e extras.