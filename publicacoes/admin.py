from django.contrib import admin
from .models import Publicacoes


class PublicacoesAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'autor_principal', 'tipo_publicacao', 'ano']
    search_fields = ['tipo_publicacao']

admin.site.register(Publicacoes, PublicacoesAdmin)
