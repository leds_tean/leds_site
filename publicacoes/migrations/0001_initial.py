# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Publicacoes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_publicacao', models.IntegerField(blank=True, choices=[(0, 'TCC'), (1, 'Periodicos'), (2, 'Anais de Congresso')], default=2, verbose_name='Tipo de Publicação')),
                ('tipo_artigo', models.IntegerField(blank=True, choices=[(0, 'Artigos Completos'), (1, 'Resumo'), (2, 'Relatorios de Conclução')], default=2, verbose_name='Tipo de Artigo')),
                ('referencia', models.CharField(max_length=400, verbose_name='Referencia')),
                ('ano', models.IntegerField(blank=True, choices=[(2015, '2015'), (2014, '2014'), (2013, '2013'), (2012, '2012'), (2011, '2011'), (2010, '2010')], verbose_name='Data da Publicação')),
                ('arquivo', models.FileField(upload_to='media/pdf/%Y/%m/%d', verbose_name='Arquivo')),
            ],
            options={
                'ordering': ['-ano'],
                'verbose_name': 'Publicacao',
                'verbose_name_plural': 'Publicações',
            },
            bases=(models.Model,),
        ),
    ]
