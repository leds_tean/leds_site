# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('publicacoes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='publicacoes',
            name='autor_principal',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1, verbose_name='Autor Principal', related_name='autor_principal'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='publicacoes',
            name='autor_secundario',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='Co-autores'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='publicacoes',
            name='titulo',
            field=models.CharField(max_length=100, default='', verbose_name='Titulo da Publicação'),
            preserve_default=False,
        ),
    ]
