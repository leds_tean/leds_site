# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('publicacoes', '0004_auto_20150513_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publicacoes',
            name='autor_secundario',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='co-autor', blank=True, verbose_name='Co-autores'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='publicacoes',
            name='local_evento',
            field=models.CharField(blank=True, verbose_name='Local do evento', max_length=100),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='publicacoes',
            name='nome_evento',
            field=models.CharField(blank=True, verbose_name='Nome do Evento', max_length=100),
            preserve_default=True,
        ),
    ]
