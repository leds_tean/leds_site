# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('publicacoes', '0002_auto_20150512_2329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publicacoes',
            name='autor_secundario',
            field=models.ManyToManyField(related_name='co-autor', verbose_name='Co-autores', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
