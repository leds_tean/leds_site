# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicacoes', '0003_auto_20150512_2330'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='publicacoes',
            name='referencia',
        ),
        migrations.AddField(
            model_name='publicacoes',
            name='local_evento',
            field=models.CharField(default='', max_length=100, verbose_name='Local do evento'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='publicacoes',
            name='nome_evento',
            field=models.CharField(default='', max_length=100, verbose_name='Nome do Evento'),
            preserve_default=False,
        ),
    ]
