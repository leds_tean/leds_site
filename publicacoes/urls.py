from django.conf.urls import patterns, url

urlpatterns = patterns('publicacoes.views',
    url(r'^$', 'publicacoes', name='publicacoes'),
    url(r'^submeter-publicacoes/$', 'submeter_publicacao', name='submit-publicacoes'),
)