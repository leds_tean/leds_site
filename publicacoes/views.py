from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import Publicacoes
from publicacoes.forms import PublicacoesForm


@login_required
def submeter_publicacao(request):
    if request.method == 'POST':
        form = PublicacoesForm(request.POST, request.FILES)
        if form.is_valid():
            public_model = Publicacoes()
            public_model.titulo = form.cleaned_data['titulo']
            public_model.tipo_publicacao = form.cleaned_data['tipo_publicacao']
            public_model.tipo_artigo = form.cleaned_data['tipo_artigo']
            public_model.nome_evento = form.cleaned_data['nome_evento']
            public_model.local_evento = form.cleaned_data['local_evento']
            public_model.ano = form.cleaned_data['ano']
            public_model.arquivo = request.FILES['arquivo']
            public_model.autor_principal = request.user
            public_model.save()
            public_model.autor_secundario = form.cleaned_data['co_autores']
            public_model.save()
            form = PublicacoesForm()
            messages.success(request, "Publicação Enviada com sucesso!")
    else:
        form = PublicacoesForm()
    template = 'submit_pbc.html'
    context = {'form': form}

    return render(request, template, context)


def publicacoes(request):
    public_all = Publicacoes.objects.all()
    template_name = 'publicacoes.html'
    context = {'publicacoes': public_all}

    return render(request, template_name, context)
