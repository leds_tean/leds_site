from django.db import models
from membros.models import Associados


class Publicacoes(models.Model):
    TIPO_PUBLICACOES_CHOICES = ((0, 'TCC'), (1, 'Periodicos'), (2, 'Anais de Congresso'))
    TIPO_ARTIGO_CHOICES = ((0, 'Artigos Completos'), (1, 'Resumo'), (2, 'Relatorios de Conclução'))
    ANO_CHOICES = ((2015, '2015'), (2014, '2014'), (2013, '2013'), (2012, '2012'), (2011, '2011'), (2010, '2010'))

    titulo = models.CharField('Titulo da Publicação', max_length=100)
    tipo_publicacao = models.IntegerField('Tipo de Publicação', choices=TIPO_PUBLICACOES_CHOICES, default=2, blank=True)
    tipo_artigo = models.IntegerField('Tipo de Artigo', choices=TIPO_ARTIGO_CHOICES, default=2, blank=True)
    nome_evento = models.CharField('Nome do Evento', max_length=100, blank=True)
    local_evento = models.CharField('Local do evento', max_length=100, blank=True)
    ano = models.IntegerField('Data da Publicação', choices=ANO_CHOICES, blank=True)
    arquivo = models.FileField('Arquivo', upload_to='media/pdf/%Y/%m/%d')
    autor_principal = models.ForeignKey(Associados, verbose_name='Autor Principal', related_name='autor_principal')
    autor_secundario = models.ManyToManyField(Associados, verbose_name='Co-autores', related_name='co-autor', blank=True)

    def get_year(self):
        return self.ano

    class Meta:
        verbose_name = 'Publicacao'
        verbose_name_plural = 'Publicações'
        ordering = ['-ano']