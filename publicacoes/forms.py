from django import forms
from membros.models import Associados


class PublicacoesForm(forms.Form):
    TIPO_PUBLICACOES_CHOICES = ((0, 'TCC'), (1, 'Periodicos'), (2, 'Anais de Congresso'))
    TIPO_ARTIGO_CHOICES = ((0, 'Artigos Completos'), (1, 'Resumo'), (2, 'Relatorios de Conclução'))
    ANO_CHOICES = ((2015, '2015'), (2014, '2014'), (2013, '2013'), (2012, '2012'), (2011, '2011'), (2010, '2010'))

    titulo = forms.CharField(label='Titulo da Publicação', max_length=100,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
    tipo_publicacao = forms.ChoiceField(label='Tipo de Publicação', choices=TIPO_PUBLICACOES_CHOICES,
                                        widget=forms.Select(attrs={'class': 'form-control'}))
    tipo_artigo = forms.ChoiceField(label='Tipo de Artigo', choices=TIPO_ARTIGO_CHOICES,
                                    widget=forms.Select(attrs={'class': 'form-control'}))
    nome_evento = forms.CharField(label='Nome do Evento', max_length=100,
                                  widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    local_evento = forms.CharField(label='Local do Evento', max_length=100,
                                  widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    ano = forms.ChoiceField(label='Data da Publicação', choices=ANO_CHOICES,
                            widget=forms.Select(attrs={'class': 'form-control'}))
    co_autores = forms.ModelMultipleChoiceField(queryset=Associados.objects.all(), required=False,
                                                widget=forms.SelectMultiple(attrs={'class': 'form-control'}))
    arquivo = forms.FileField(label='Selecione um arquivo',
                              widget=forms.FileInput(attrs={'accept': 'application/pdf'}))
