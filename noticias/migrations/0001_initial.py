# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Noticias',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('titulo', models.CharField(max_length=250, verbose_name='Titulo')),
                ('slug', models.SlugField(max_length=100, verbose_name='Atalho', blank=True, unique=True)),
                ('descricao', models.TextField(verbose_name='Descrição da Noticia', blank=True)),
                ('sobre', redactor.fields.RedactorField(verbose_name='Texto')),
                ('image', models.ImageField(null=True, verbose_name='Imagem', upload_to='noticias/images', blank=True)),
                ('created_at', models.DateTimeField(verbose_name='Criado em', auto_now_add=True)),
                ('updated_at', models.DateTimeField(verbose_name='Atualizado em', auto_now=True)),
            ],
            options={
                'ordering': ['-created_at'],
                'verbose_name': 'Noticia',
                'verbose_name_plural': 'Noticias',
            },
            bases=(models.Model,),
        ),
    ]
