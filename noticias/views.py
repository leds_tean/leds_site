from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Noticias


def noticias(request):
    noticias_home = Noticias.objects.all()
    template_name = 'noticiasIndex.html'
    context = {
        'noticias': noticias_home
    }
    return render(request, template_name, context)


def descricao(request, slug=None):
    pagina_description = get_object_or_404(Noticias, slug=slug)
    context = {'noticias': pagina_description}
    template_name = 'pagNoticia.html'
    return render(request, template_name, context)


def lista_noticias(request):
    noticias = Noticias.objects.all()
    ultimas_noticias = Noticias.objects.all()[:3]#retorna as tres ultimas noticias
    paginator = Paginator(noticias, 10)

    page = request.GET.get('page')
    try:
        documentos = paginator.page(page)
    except PageNotAnInteger:
        documentos = paginator.page(1)
    except EmptyPage:
        documentos = paginator.page(paginator.num_pages)

    template = 'noticiasIndex.html'
    context = {'noticias': documentos, 'paginator': paginator, 'ultimas_noticias': ultimas_noticias}
    return render(request, template, context)