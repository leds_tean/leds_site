from django.conf.urls import patterns, url

urlpatterns = patterns('noticias.views',
    url(r'^$', 'lista_noticias', name='index'),
    url(r'^noticias/', 'descricao', name='noticias'),
    url(r'^(?P<slug>[\w_-]+)/$', 'descricao', name='descricao'),


)