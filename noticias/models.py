from redactor.fields import RedactorField
from django.db import models
from django.contrib.auth.models import User


class NoticiaManager(models.Manager):
    def search(self, query):
        return self.get_queryset().filter(models.Q(titulo_icontains=query) | models.Q(descricao_icontains=query))


class Noticias(models.Model):
    titulo = models.CharField(max_length=250, verbose_name=u'Titulo')
    slug = models.SlugField("Atalho", max_length=100, blank=True, unique=True)
    descricao = models.TextField("Descrição da Noticia", blank=True)
    sobre = RedactorField(verbose_name=u'Texto', redactor_options={'plugins': ['table']})
    image = models.ImageField(upload_to='noticias/images', verbose_name='Imagem', null=True, blank=True)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo

    @models.permalink
    def get_absolute_url(self):
        return 'noticias:descricao', (), {'slug': self.slug}

    class Meta:
        verbose_name = 'Noticia'
        verbose_name_plural = 'Noticias'
        ordering = ['-created_at']
