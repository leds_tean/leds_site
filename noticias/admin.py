from django.contrib import admin
from django import forms
from redactor.widgets import RedactorEditor
from noticias.models import Noticias


class EntryAdminForm(forms.ModelForm):
    class Meta:
        model = Noticias
        widgets = {
            'sobre': RedactorEditor(),
            'descricao': RedactorEditor(),
        }


class EntryAdminDescricao(admin.ModelAdmin):
    form = EntryAdminForm


class NoticiasAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'sobre', 'slug', 'descricao', 'created_at', 'updated_at']
    search_fields = ['titulo']
    prepopulated_fields = {'slug': ("titulo",)}


admin.site.register(Noticias, NoticiasAdmin)



