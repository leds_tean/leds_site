from django import forms


class RelatorioForm(forms.Form):
    participantes = forms.CharField(label='Nome dos Participantes', max_length=400,
                                    widget=forms.TextInput(attrs={'class': 'form-control'}))
    texto = forms.CharField(label='Descrição do Documento', required=False,
                            widget=forms.Textarea(attrs={'class': 'form-control'}))