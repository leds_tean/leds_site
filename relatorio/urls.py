from django.conf.urls import patterns, include, url

urlpatterns = patterns('relatorio.views',
    url(r'^ata/$','relatorio', name='escrever-ata'),
    url(r'^listar_atas/$','relatorio_lista', name='listar_atas'),
    url(r'^listar_atas/(?P<pk>\d+)/$', 'ver_ata', name='ver_ata'),
)
