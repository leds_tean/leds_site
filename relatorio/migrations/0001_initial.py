# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Relatorio',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('paticipantes', models.CharField(max_length=400, verbose_name='Participantes')),
                ('texto', models.TextField(blank=True, verbose_name='Contexto')),
                ('data_Criacao', models.DateTimeField(auto_now_add=True, verbose_name='Data de Criação')),
            ],
            options={
                'verbose_name_plural': 'Relatorios',
                'ordering': ['data_Criacao'],
                'verbose_name': 'Relatorio',
            },
            bases=(models.Model,),
        ),
    ]
