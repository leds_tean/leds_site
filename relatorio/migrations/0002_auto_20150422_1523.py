# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('relatorio', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='relatorio',
            old_name='paticipantes',
            new_name='participantes',
        ),
    ]
