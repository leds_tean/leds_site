from django.db import models

class RelatorioManager(models.Manager):
	def search(self, query):
		return self.get_queryset().filter(
			models.Q(name__icontains=query)| \
			models.Q(description__icontains=query)
		)

class Relatorio(models.Model):	
	participantes = models.CharField('Participantes', max_length=400)
	texto = models.TextField('Contexto', blank=True)
	data_Criacao = models.DateTimeField('Data de Criação', auto_now_add=True)

	objects=RelatorioManager()

	@models.permalink
	def get_absolute_url(self):
		return ('relatorio:ver_ata', (), {'pk': self.pk})

	class Meta:
		verbose_name = 'Relatorio'
		verbose_name_plural = 'Relatorios'
		ordering = ['data_Criacao']
