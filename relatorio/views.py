from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from relatorio.forms import RelatorioForm
from relatorio.models import Relatorio

@login_required
def relatorio(request):
    if request.method == 'POST':
        form = RelatorioForm(request.POST)
        if form.is_valid():
            model_doc = Relatorio()
            model_doc.participantes = form.cleaned_data['participantes']
            model_doc.texto = form.cleaned_data['texto']
            print(model_doc.participantes)
            model_doc.save()
            form = RelatorioForm()
    else:
        form = RelatorioForm()
    template = 'escrever-ata.html'
    context = {'form': form}

    return render(request, template, context)

def relatorio_lista(request):
	atas = Relatorio.objects.all()
	template_name = 'listar_atas.html'
	context = {
		'lista' : atas
	}
	return render(request, template_name, context)

def ver_ata(request, pk):
	ata = get_object_or_404(Relatorio, pk=pk)
	template_name = 'ver_ata.html'
	context = {
		'ver_ata' : ata
	}
	return render(request, template_name, context)
