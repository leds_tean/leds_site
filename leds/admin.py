from django.contrib import admin

from .models import Dados


class DadosAdmin(admin.ModelAdmin):
    list_display = ['titulo', 'texto', 'data_Criacao']
    search_fields = ['titulo']

admin.site.register(Dados, DadosAdmin)