# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dados',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200, verbose_name='Titulo')),
                ('texto', models.TextField(blank=True, verbose_name='Contexto')),
                ('data_Criacao', models.DateTimeField(auto_now_add=True, verbose_name='Data de Criação')),
                ('data_Atualizacao', models.DateTimeField(auto_now=True, verbose_name='Atalizado em')),
            ],
            options={
                'verbose_name_plural': 'Quem Somos',
                'ordering': ['data_Criacao'],
                'verbose_name': 'Quem Somos',
            },
            bases=(models.Model,),
        ),
    ]
