from django.shortcuts import render, get_object_or_404
from .models import Dados
from .forms import LedsForm


def ver_dados(request):
    dados = Dados.objects.all()
    dados = get_object_or_404(Dados, pk=(len(dados)))
    context = {}
    if request.method == 'POST':
        form = LedsForm(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            form.send_mail()
            form = LedsForm()
    else:
        form = LedsForm()
    template_name = 'leds.html'
    context['dados'] = dados
    context['form'] = form

    return render(request, template_name, context)
