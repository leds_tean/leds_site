from django.db import models

class DadosManager(models.Manager):
	def search(self, query):
		return self.get_queryset().filter(
			models.Q(name__icontains=query)| \
			models.Q(description__icontains=query)
		)

class Dados(models.Model):	
	titulo = models.CharField('Titulo', max_length=200)
	texto = models.TextField('Contexto', blank=True)
	data_Criacao = models.DateTimeField('Data de Criação', auto_now_add=True)
	data_Atualizacao = models.DateTimeField('Atalizado em', auto_now=True)
	
	objects=DadosManager()

	@models.permalink
	def get_absolute_url(self):
		return ('quemSomos:leds', (), {'pk': self.pk})

	def __str__(self):
		return self.titulo

	class Meta:
		verbose_name = 'Quem Somos'
		verbose_name_plural = 'Quem Somos'
		ordering = ['data_Criacao']
		
