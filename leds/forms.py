from django import forms
from django.core.mail import send_mail
from django.conf import settings


class LedsForm(forms.Form):
    nome = forms.CharField(label='Nome', widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=400)
    email = forms.EmailField(label='E-mail', widget=forms.TextInput(attrs={'class': 'form-control'}))
    texto = forms.CharField(label='Menssagem ou Duvida', widget=forms.Textarea(attrs={'class': 'form-control'}))

    def send_mail(self):
        subject = 'Informes'
        menssagem = 'Nome: %(nome)s; E-mail: %(email)s; Menssagem: %(texto)s'
        context = {
            'nome': self.cleaned_data['nome'],
            'email': self.cleaned_data['email'],
            'texto': self.cleaned_data['texto'],
        }
        menssagem = menssagem % context
        send_mail(subject, menssagem, settings.DEFAULT_FROM_EMAIL, [settings.CONTACT_EMAIL])